variable "profile" {
  description = "Stop! Clone the model and set the profile name for the member account"
}

variable "audit_s3_bucket_name" {
  description = "The name of the S3 bucket to store various audit logs."
  default = "isme-baseline-audit-1"
}

variable "region" {
  description = "The AWS region in which global resources are set up."
  default     = "eu-west-1"
}

variable "target_regions" {
  description = "A list of regions to set up with this module."
  default = ["eu-west-1", "us-east-1"]
}
