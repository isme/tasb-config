terraform {
  backend "s3" {
      profile = "saa-sso"
      region = "eu-west-1"
      role_arn = "arn:aws:iam::192985681585:role/TerraformBackend-Role-13HCYDOTG4GK2"
      bucket = "terraformbackend-bucket-xuao5rjmgc2d"
      key = "config/sso/test_encryption_4.tfstate"
      acl = "bucket-owner-full-control"
      encrypt = "true"
      kms_key_id = "arn:aws:kms:eu-west-1:192985681585:key/e43a7dde-79f2-493f-8c9b-23056b767bed"
      dynamodb_table = "TerraformBackend-Table-KG7LA5ZHKDY2"
  }
}
