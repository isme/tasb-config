#!/bin/bash
set -euxo pipefail

script_dir=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

module="${1}"

(cd "${module}" && rm -rf .terraform/modules/secure_baseline && terraform init)

rename.ul .tf .tf.disabled "${module}/.terraform/modules/secure_baseline/vpc_baselines.tf"
rename.ul .tf .tf.disabled "${module}/.terraform/modules/secure_baseline/outputs.tf"
cp "${script_dir}/module_injections/outputs.tf" "${module}/.terraform/modules/secure_baseline/outputs.tf"
rename.ul .tf .tf.disabled "${module}/.terraform/modules/secure_baseline/ebs_baselines.tf"
rename.ul .tf .tf.disabled "${module}/.terraform/modules/secure_baseline/analyzer_baselines.tf"
rename.ul .tf .tf.disabled "${module}/.terraform/modules/secure_baseline/securityhub_baselines.tf"
rename.ul .tf .tf.disabled "${module}/.terraform/modules/secure_baseline/guardduty_baselines.tf"
rename.ul .tf .tf.disabled "${module}/.terraform/modules/secure_baseline/main.tf"
cp "${script_dir}/module_injections/main.tf" "${module}/.terraform/modules/secure_baseline/main.tf"
