# --------------------------------------------------------------------------------------------------
# Outputs from the root module.
# --------------------------------------------------------------------------------------------------

output "audit_bucket" {
  description = "The S3 bucket used for storing audit logs."
  value       = module.audit_log_bucket.this_bucket
}

# --------------------------------------------------------------------------------------------------
# Outputs from config-baseline module.
# --------------------------------------------------------------------------------------------------

output "config_iam_role" {
  description = "The IAM role used for delivering AWS Config records to CloudWatch Logs."
  value       = aws_iam_role.recorder
}

output "config_configuration_recorder" {
  description = "The configuration recorder in each region."

  value = {
    "ap-northeast-1" = module.config_baseline_ap-northeast-1.configuration_recorder
    "ap-northeast-2" = module.config_baseline_ap-northeast-2.configuration_recorder
    "ap-south-1"     = module.config_baseline_ap-south-1.configuration_recorder
    "ap-southeast-1" = module.config_baseline_ap-southeast-1.configuration_recorder
    "ap-southeast-2" = module.config_baseline_ap-southeast-2.configuration_recorder
    "ca-central-1"   = module.config_baseline_ca-central-1.configuration_recorder
    "eu-central-1"   = module.config_baseline_eu-central-1.configuration_recorder
    "eu-west-1"      = module.config_baseline_eu-west-1.configuration_recorder
    "eu-west-2"      = module.config_baseline_eu-west-2.configuration_recorder
    "eu-west-3"      = module.config_baseline_eu-west-3.configuration_recorder
    "sa-east-1"      = module.config_baseline_sa-east-1.configuration_recorder
    "us-east-1"      = module.config_baseline_us-east-1.configuration_recorder
    "us-east-2"      = module.config_baseline_us-east-2.configuration_recorder
    "us-west-1"      = module.config_baseline_us-west-1.configuration_recorder
    "us-west-2"      = module.config_baseline_us-west-2.configuration_recorder
  }
}

output "config_sns_topic" {
  description = "The SNS topic that AWS Config delivers notifications to."

  value = {
    "ap-northeast-1" = module.config_baseline_ap-northeast-1.config_sns_topic
    "ap-northeast-2" = module.config_baseline_ap-northeast-2.config_sns_topic
    "ap-south-1"     = module.config_baseline_ap-south-1.config_sns_topic
    "ap-southeast-1" = module.config_baseline_ap-southeast-1.config_sns_topic
    "ap-southeast-2" = module.config_baseline_ap-southeast-2.config_sns_topic
    "ca-central-1"   = module.config_baseline_ca-central-1.config_sns_topic
    "eu-central-1"   = module.config_baseline_eu-central-1.config_sns_topic
    "eu-north-1"     = module.config_baseline_eu-north-1.config_sns_topic
    "eu-west-1"      = module.config_baseline_eu-west-1.config_sns_topic
    "eu-west-2"      = module.config_baseline_eu-west-2.config_sns_topic
    "eu-west-3"      = module.config_baseline_eu-west-3.config_sns_topic
    "sa-east-1"      = module.config_baseline_sa-east-1.config_sns_topic
    "us-east-1"      = module.config_baseline_us-east-1.config_sns_topic
    "us-east-2"      = module.config_baseline_us-east-2.config_sns_topic
    "us-west-1"      = module.config_baseline_us-west-1.config_sns_topic
    "us-west-2"      = module.config_baseline_us-west-2.config_sns_topic
  }
}
