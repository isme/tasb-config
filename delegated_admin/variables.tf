variable "profile" {
  description = "The CLI profile of the delegated administrator of AWS Config"
  type = string
  default = "saa-secdel"
}

variable "audit_s3_bucket_name" {
  description = "The name of the S3 bucket to store various audit logs."
  type = string
  default = "isme-baseline-audit-1"
}

variable "region" {
  description = "The AWS region in which global resources are set up."
  type = string
  default     = "eu-west-1"
}

variable "target_regions" {
  description = "A list of regions to set up with this module."
  type = list(string)
  default = ["eu-west-1", "us-east-1"]
}

variable "member_accounts" {
  description = "A list of IDs and emails of AWS accounts which associated as member accounts."
  type = list(object({
    account_id = string
    email      = string
  }))
  default     = [
    {
      account_id = "638726906110",
      email = ""
    },
    {
      account_id = "480783779961",
      email = ""
    },
    {
      account_id = "423811555754",
      email = ""
    }
  ]
}
