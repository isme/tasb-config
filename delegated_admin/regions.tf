# --------------------------------------------------------------------------------------------------
# A list of providers for all AWS regions.
# Reference: https://docs.aws.amazon.com/general/latest/gr/rande.html
# --------------------------------------------------------------------------------------------------

provider "aws" {
  profile = var.profile
  region  = "ap-northeast-1"
  alias   = "ap-northeast-1"
}

provider "aws" {
  profile = var.profile
  region  = "ap-northeast-2"
  alias   = "ap-northeast-2"
}

provider "aws" {
  profile = var.profile
  region  = "ap-northeast-3"
  alias   = "ap-northeast-3"
}

provider "aws" {
  profile = var.profile
  region  = "ap-south-1"
  alias   = "ap-south-1"
}

provider "aws" {
  profile = var.profile
  region  = "ap-southeast-1"
  alias   = "ap-southeast-1"
}

provider "aws" {
  profile = var.profile
  region  = "ap-southeast-2"
  alias   = "ap-southeast-2"
}

provider "aws" {
  profile = var.profile
  region  = "ca-central-1"
  alias   = "ca-central-1"
}

provider "aws" {
  profile = var.profile
  region  = "eu-central-1"
  alias   = "eu-central-1"
}

provider "aws" {
  profile = var.profile
  region  = "eu-north-1"
  alias   = "eu-north-1"
}

provider "aws" {
  profile = var.profile
  region  = "eu-west-1"
  alias   = "eu-west-1"
}

provider "aws" {
  profile = var.profile
  region  = "eu-west-2"
  alias   = "eu-west-2"
}

provider "aws" {
  profile = var.profile
  region  = "eu-west-3"
  alias   = "eu-west-3"
}

provider "aws" {
  profile = var.profile
  region  = "sa-east-1"
  alias   = "sa-east-1"
}

provider "aws" {
  profile = var.profile
  region  = "us-east-1"
  alias   = "us-east-1"
}

provider "aws" {
  profile = var.profile
  region  = "us-east-2"
  alias   = "us-east-2"
}

provider "aws" {
  profile = var.profile
  region  = "us-west-1"
  alias   = "us-west-1"
}

provider "aws" {
  profile = var.profile
  region  = "us-west-2"
  alias   = "us-west-2"
}

