provider "aws" {
  profile = var.profile
  region  = var.region
}

data "aws_caller_identity" "current" {
}

# API calls required by Terraform to read bucket metadata.
# See https://github.com/hashicorp/terraform-provider-aws/blob/4425957ef58f94061f92a45e694ff6ddb56cfe3b/aws/resource_aws_s3_bucket.go
data "aws_iam_policy_document" "terraform_metadata" {

  dynamic "statement" {
    for_each = [var.member_accounts]

    content {
      sid = "TerraformBucketMetadataReadForMemberAccounts"
      actions = [
        "s3:ListBucket",
        "s3:GetBucketPolicy",
        "s3:GetBucketAcl",
        "s3:GetBucketAcl",
        "s3:GetBucketCORS",
        "s3:GetBucketWebsite",
        "s3:GetBucketVersioning",
        "s3:GetLifecycleConfiguration",
        "s3:GetReplicationConfiguration",
        "s3:GetBucketObjectLockConfiguration",
        "s3:GetBucketTagging"
      ]
      principals {
        type = "AWS"
        identifiers = [for account in statement.value : "arn:aws:iam::${account.account_id}:root"]
      }
      resources = ["arn:aws:s3:::${var.audit_s3_bucket_name}"]
    }
  }
}

module "secure_baseline" {
  source = "nozaq/secure-baseline/aws"
  version = "0.23.1"

  aws_account_id                       = data.aws_caller_identity.current.account_id
  account_type                         = "master"
  region                               = var.region
  member_accounts                      = var.member_accounts
  target_regions                       = var.target_regions
  audit_log_bucket_name                = var.audit_s3_bucket_name
  audit_log_bucket_custom_policy_json  = data.aws_iam_policy_document.terraform_metadata.json
  support_iam_role_principal_arns      = []

  # Setting it to true means all audit logs are automatically deleted
  #   when you run `terraform destroy`.
  # Note that it might be inappropriate for highly secured environment.
  audit_log_bucket_force_destroy = true

  providers = {
    aws                = aws
    aws.ap-northeast-1 = aws.ap-northeast-1
    aws.ap-northeast-2 = aws.ap-northeast-2
    aws.ap-northeast-3 = aws.ap-northeast-3
    aws.ap-south-1     = aws.ap-south-1
    aws.ap-southeast-1 = aws.ap-southeast-1
    aws.ap-southeast-2 = aws.ap-southeast-2
    aws.ca-central-1   = aws.ca-central-1
    aws.eu-central-1   = aws.eu-central-1
    aws.eu-north-1     = aws.eu-north-1
    aws.eu-west-1      = aws.eu-west-1
    aws.eu-west-2      = aws.eu-west-2
    aws.eu-west-3      = aws.eu-west-3
    aws.sa-east-1      = aws.sa-east-1
    aws.us-east-1      = aws.us-east-1
    aws.us-east-2      = aws.us-east-2
    aws.us-west-1      = aws.us-west-1
    aws.us-west-2      = aws.us-west-2
  }
}

