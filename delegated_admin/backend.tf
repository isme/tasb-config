terraform {
  backend "s3" {
      profile = "saa-secdel"
      region = "eu-west-1"
      role_arn = "arn:aws:iam::192985681585:role/TerraformBackend-Role-13HCYDOTG4GK2"
      bucket = "terraformbackend-bucket-xuao5rjmgc2d"
      key = "config/delegated_admin/terraform.tfsate"
      acl = "bucket-owner-full-control"
      encrypt = "true"
      dynamodb_table = "TerraformBackend-Table-KG7LA5ZHKDY2"
  }
}
