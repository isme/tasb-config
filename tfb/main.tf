provider "aws" {
  profile    = var.profile
  region     = var.region
}

provider "aws" {
  alias  = "replica"
  profile    = var.profile
  region = var.replica_region
}

module "remote_state" {
  source = "nozaq/remote-state-s3-backend/aws"
  version = ">=0.4.1"

  s3_bucket_force_destroy = var.s3_bucket_force_destroy

  providers = {
    aws         = aws
    aws.replica = aws.replica
  }
}

resource "aws_iam_user" "terraform" {
  name = "TerraformUser"
}

resource "aws_iam_user_policy_attachment" "remote_state_access" {
  user       = aws_iam_user.terraform.name
  policy_arn = module.remote_state.terraform_iam_policy.arn
}
