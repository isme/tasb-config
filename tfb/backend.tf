terraform {
  backend "s3" {
    key     = "tfb/terraform.tfstate"
    region  = "eu-west-1"
    encrypt = true
  }
}