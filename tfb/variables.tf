variable "profile" {
  type = string
}

variable "region" {
  type = string
  description = "The AWS region in which resources are set up."
  default     = "eu-west-1"
}


variable "replica_region" {
  type = string
  description = "The AWS region to which the state bucket is replicated."
  default     = "eu-west-1"
}

variable "s3_bucket_force_destroy" {
  type = bool
  description = "A boolean that indicates all objects should be deleted from S3 buckets so that the buckets can be destroyed without error. These objects are not recoverable."
  default     = false
}
