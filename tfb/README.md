# Terraform Backend (tfb)

We use nozaq's [terraform-aws-remote-state-s3-backend][nozaq] to set up the
backend.

## Creating the backend

Set the AWS CLI profile.

```
profile="saa-tfb"
```

Create the resources using a local backend.

```text
mv backend.tf backend.tf.disabled

terraform init
terraform plan -var "profile=${profile}" -out ~/tmp/tfplan
terraform apply ~/tmp/tfplan
```

If you get this error in the apply phase, plan and apply again. See
[Github issue](https://github.com/nozaq/terraform-aws-remote-state-s3-backend/issues/35).

```text
Error: Error putting S3 policy: OperationAborted: A conflicting conditional operation is currently in progress against this resource. Please try again.
	status code: 409, request id: 64594BB0B34560F3, host id: 4AoXq2VVwqUyjqdp5ZiQDh6lKrR1XEbUcYUUnQan3pwIc0Tc3eBy9w9B8c/MRCFWadtG1bz6wWA=
```

Move the state from the local backend to the S3 bucket you just created.

```
mkdir -p config

terraform output > config/backend-tfb.conf

mv backend.tf.disabled backend.tf

terraform init \
-force-copy \
-backend-config config/backend-tfb.conf
```

Remove local state files from the context to be sure you're working with the
remote state.

```
mv terraform.tfstate terraform.tfstate.backup ~/tmp
```

Check the the key in the bucket matches the value set in backend.tf.

```text
$ aws s3api list-objects --bucket "$(terraform output -raw bucket)" --profile $profile
{
    "Contents": [
        {
            "Key": "tfb/terraform.tfstate",
            "LastModified": "2021-02-16T10:45:15+00:00",
            "ETag": "\"dcad764e53aa2a300c3d52bc0c0df712\"",
            "Size": 30722,
            "StorageClass": "STANDARD",
            "Owner": {
                "DisplayName": "iain+saa-terraform-backend",
                "ID": "3ee7a2cd233dd3c491f471dc91573903e02f3ae441f31cfee4de441ab11f2351"
            }
        }
    ]
}
```

Check you can read the remote state.

```
terraform state list
```

## Deleting the backend

Move the state to the local backend.

```
mv backend.tf backend.tf.disabled

terraform init -force-copy
```

Check you can read the local state.

```
terraform state list -state terraform.tfstate
```

Allow Terraform to empty the bucket before destroying it.

```
terraform plan \
-var "profile=${profile}" \
-var s3_bucket_force_destroy=true \
-out ~/tmp/tfplan

terraform apply ~/tmp/tfplan
```

Delete the remote backend.

```
terraform plan \
-destroy \
-var "profile=${profile}" \
-out ~/tmp/tfplan

terraform apply ~/tmp/tfplan
```

If you get this error in the apply phase, plan and apply again.

```
Error: Error deleting S3 policy: OperationAborted: A conflicting conditional operation is currently in progress against this resource. Please try again.
	status code: 409, request id: A3649BA4C2972D89, host id: 82jEG5IvZZ4+8A+haw4a2+pnO7Jp3gZNXpfNv3AMXj7Uc8M+WK3EwWH9tJZLqWTsjl+xsJlVoj8=
```

If you don't need the local state any more, just delete it.

```
rm -rf .terraform terraform.tfstate terraform.tfstate.backup
```

<!-- # References -->

[nozaq]: https://github.com/nozaq/terraform-aws-remote-state-s3-backend
