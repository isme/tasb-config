# Deploying AWS Config with Terraform AWS Secure Baseline (TASB)

The terraform-aws-secure-baseline-module is great, but it does too much and I
don't see how to disable the unused features.

So first I start to hack it to pare it down to what we need.

## Master account

Iterating with this command to pare down the module:

```
terraform plan \
  -var audit_s3_bucket_name=isme-baseline-audit-1 \
  -var profile=saa-secdel-iam \
  -out ~/tmp/tfplan
```

Using the module without disabling any functionality:

> Plan: 317 to add, 0 to change, 0 to destroy.

A hacky way to see just the addresses that would be created.

```
terraform show -json ~/tmp/tfplan | gron | grep -P 'json\.resource_changes\[\d+\]\.address' | less
```

A slightly more tidy way:

```
terraform show -json ~/tmp/tfplan | jq --raw-output '.resource_changes[].address'
```

Using the module just in the global region:

> Plan: 77 to add, 0 to change, 0 to destroy.

How do we start disabling stuff?

```
$ find .terraform/modules/secure_baseline/ -mindepth 1 -maxdepth 1 -type f -name '*.tf'
.terraform/modules/secure_baseline/providers.tf
.terraform/modules/secure_baseline/config_baselines.tf
.terraform/modules/secure_baseline/main.tf
.terraform/modules/secure_baseline/bucket.tf
.terraform/modules/secure_baseline/vpc_baselines.tf
.terraform/modules/secure_baseline/versions.tf
.terraform/modules/secure_baseline/ebs_baselines.tf
.terraform/modules/secure_baseline/variables.tf
.terraform/modules/secure_baseline/analyzer_baselines.tf
.terraform/modules/secure_baseline/securityhub_baselines.tf
.terraform/modules/secure_baseline/guardduty_baselines.tf
.terraform/modules/secure_baseline/outputs.tf
```

Disable vpc_baselines.

```
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/vpc_baselines.tf
```

It's broken by missing outputs.

Create a new file in our client code with just the inputs we need.

Start with a copy of the module's outputs file and delete everything that isn't needed.

```
cp .terraform/modules/secure_baseline/outputs.tf outputs.tf.module
```

Keep the outputs under the headings for the root module and the config-baseline
module. Delete everything else.

Disable the original outputs.

```
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/outputs.tf
```

Inject just the inputs we need.

```
cp outputs.tf.module .terraform/modules/secure_baseline/outputs.tf
```

Iterate again.

Using the module in just the global region with just the inputs we need:

> Plan: 69 to add, 0 to change, 0 to destroy.

Disable ebs_baselines.

```
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/ebs_baselines.tf
```

Iterate.

> Plan: 68 to add, 0 to change, 0 to destroy.

Disable everything else that isn't config!

```
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/analyzer_baselines.tf
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/securityhub_baselines.tf
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/guardduty_baselines.tf
```

Iterate.

> Plan: 63 to add, 0 to change, 0 to destroy.

Why still so many things to be added?

Because the main.tf calls three submodules more.

We can't just disable the file because it contains some locals that we need.

Create a new file in our client code with just the locals we need.

Start with a copy of the module's main file and delete everything that isn't needed.

```
cp .terraform/modules/secure_baseline/main.tf main.tf.module
```

Keep the locals declarations. Delete all the module invocations.

Disable the original main.

```
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/main.tf
```

Inject just the inputs we need.

```
cp main.tf.module .terraform/modules/secure_baseline/main.tf
```

Iterate.

> Plan: 22 to add, 0 to change, 0 to destroy.

This looks better. What are they?

```
$ terraform show -json ~/tmp/tfplan | jq --raw-output '.resource_changes[].address'
aws_iam_user.admin
aws_organizations_organization.org
module.secure_baseline.aws_config_config_rule.iam_mfa
module.secure_baseline.aws_config_config_rule.no_policies_with_full_admin_access
module.secure_baseline.aws_config_config_rule.unused_credentials
module.secure_baseline.aws_config_config_rule.user_no_policies
module.secure_baseline.aws_config_configuration_aggregator.organization[0]
module.secure_baseline.aws_iam_role.config_organization[0]
module.secure_baseline.aws_iam_role.recorder
module.secure_baseline.aws_iam_role_policy.recorder_publish_policy
module.secure_baseline.aws_iam_role_policy_attachment.config_organization[0]
module.secure_baseline.aws_iam_role_policy_attachment.recorder_read_policy
module.secure_baseline.aws_s3_bucket_policy.audit_log[0]
module.secure_baseline.data.aws_iam_policy_document.audit_log[0]
module.secure_baseline.data.aws_iam_policy_document.audit_log_base[0]
module.secure_baseline.data.aws_iam_policy_document.audit_log_cloud_trail[0]
module.secure_baseline.data.aws_iam_policy_document.audit_log_config[0]
module.secure_baseline.data.aws_iam_policy_document.recorder_publish_policy
module.secure_baseline.module.audit_log_bucket.aws_s3_bucket.access_log[0]
module.secure_baseline.module.audit_log_bucket.aws_s3_bucket.content[0]
module.secure_baseline.module.audit_log_bucket.aws_s3_bucket_policy.access_log_policy[0]
module.secure_baseline.module.audit_log_bucket.aws_s3_bucket_public_access_block.access_log[0]
module.secure_baseline.module.audit_log_bucket.aws_s3_bucket_public_access_block.content[0]
module.secure_baseline.module.audit_log_bucket.data.aws_iam_policy_document.access_log_policy[0]
module.secure_baseline.module.config_baseline_us-east-1.aws_config_configuration_recorder.recorder[0]
module.secure_baseline.module.config_baseline_us-east-1.aws_config_configuration_recorder_status.recorder[0]
module.secure_baseline.module.config_baseline_us-east-1.aws_config_delivery_channel.bucket[0]
module.secure_baseline.module.config_baseline_us-east-1.aws_sns_topic.config[0]
```

The module would now only add resources for AWS Config.

The example code that drives the whole process creates an IAM user and a
organization that we don't want. It also passes some arguments that the module
no longer uses.

The organization is already created and managed outside Terraform. Besides, the
account we will use as the AWS Config "master" account is actually a delegated
administrator account, so we can't define member accounts here.

The IAM user is for AWS Support role compliance.

The GuardDuty argument doesn't make sense after disabling its submodule.

The member_accounts argument isn't used by the AWS Config submodule. (*Wrong!
Later on we'll see why.*) The aggregator automatically reads from all
organization accounts.

Delete the unwanted resources and remove the unused arguments to the module.

The support_iam_role_principal_arns is required so set it to an empty list.

Iterate.

> Plan: 20 to add, 0 to change, 0 to destroy.

What are they?

```
$ terraform show -json ~/tmp/tfplan | jq --raw-output '.resource_changes[].address'
module.secure_baseline.aws_config_config_rule.iam_mfa
module.secure_baseline.aws_config_config_rule.no_policies_with_full_admin_access
module.secure_baseline.aws_config_config_rule.unused_credentials
module.secure_baseline.aws_config_config_rule.user_no_policies
module.secure_baseline.aws_config_configuration_aggregator.organization[0]
module.secure_baseline.aws_iam_role.config_organization[0]
module.secure_baseline.aws_iam_role.recorder
module.secure_baseline.aws_iam_role_policy.recorder_publish_policy
module.secure_baseline.aws_iam_role_policy_attachment.config_organization[0]
module.secure_baseline.aws_iam_role_policy_attachment.recorder_read_policy
module.secure_baseline.aws_s3_bucket_policy.audit_log[0]
module.secure_baseline.data.aws_iam_policy_document.audit_log[0]
module.secure_baseline.data.aws_iam_policy_document.audit_log_base[0]
module.secure_baseline.data.aws_iam_policy_document.audit_log_cloud_trail[0]
module.secure_baseline.data.aws_iam_policy_document.audit_log_config[0]
module.secure_baseline.data.aws_iam_policy_document.recorder_publish_policy
module.secure_baseline.module.audit_log_bucket.aws_s3_bucket.access_log[0]
module.secure_baseline.module.audit_log_bucket.aws_s3_bucket.content[0]
module.secure_baseline.module.audit_log_bucket.aws_s3_bucket_policy.access_log_policy[0]
module.secure_baseline.module.audit_log_bucket.aws_s3_bucket_public_access_block.access_log[0]
module.secure_baseline.module.audit_log_bucket.aws_s3_bucket_public_access_block.content[0]
module.secure_baseline.module.audit_log_bucket.data.aws_iam_policy_document.access_log_policy[0]
module.secure_baseline.module.config_baseline_us-east-1.aws_config_configuration_recorder.recorder[0]
module.secure_baseline.module.config_baseline_us-east-1.aws_config_configuration_recorder_status.recorder[0]
module.secure_baseline.module.config_baseline_us-east-1.aws_config_delivery_channel.bucket[0]
module.secure_baseline.module.config_baseline_us-east-1.aws_sns_topic.config[0]
```

Looks good.

Aside: What does "[0]" mean in
"module.secure_baseline.aws_config_configuration_aggregator.organization[0]"?

It's because count is used internally to optionally create the resource.

https://www.terraform.io/docs/language/meta-arguments/count.html

```terraform
resource "aws_config_configuration_aggregator" "organization" {
  count = local.is_master_account ? 1 : 0
[...]
```

## Reverting to the original module

What happens to the hacked module if we reinit?

The modified code is not updated.

You need to remove the folder containing the module's code to make init download
the original code again.

mv .terraform/modules/secure_baseline/ ~/tmp/secure_baseline_hacked

Iterate again.

> Plan: 75 to add, 0 to change, 0 to destroy.

Which makes sense: the 77 from running in a single region minus the 2 removed
from the example code.

## Patching it again quickly

Let's put it all together.

First refactor to make it easier to reuse in the member account.

```
$ cd ..
$ mkdir module_injections
$ mv master/main.tf.module module_injections/main.tf
$ mv master/outputs.tf.module module_injections/outputs.tf
```

Adapt the commands for the new structure.

```
cd master
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/vpc_baselines.tf
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/outputs.tf
cp ../module_injections/outputs.tf .terraform/modules/secure_baseline/outputs.tf
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/ebs_baselines.tf
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/analyzer_baselines.tf
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/securityhub_baselines.tf
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/guardduty_baselines.tf
rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/main.tf
cp ../module_injections/main.tf .terraform/modules/secure_baseline/main.tf
```

Iterate.

> Plan: 20 to add, 0 to change, 0 to destroy.

Good! Now we have a quick patching process to pare down the module to AWS Config.

## Member account

Init and patch.

Edit the member main using what we learned from the master account.

* Remove unused module arguments and data sources
* Remove unwanted resources

Now iterate with this command:

```
terraform plan \
  -var audit_s3_bucket_name=isme-baseline-audit-1 \
  -var profile=saa-mem1-iam \
  -out ~/tmp/tfplan
```

Remove the master_account_id variable from the member account driver. The value
is needed only for GuardDuty, which is disabled. In the main.tf it wasn't used
at all because instead it was was derived from the organization's management
account using a data source.

Iterate again.

We see an error because the audit bucket does not exist.

```
Error: Failed getting S3 bucket: NotFound: Not Found
	status code: 404, request id: E9501E75AD2EF39C, host id: 113bwkZmS8osMXaCFOykiqWdKBGRPWe2IiADK83D634K9Pnfxl/GhBHmlccidOKnTmazLktTjg0= Bucket: "isme-baseline-audit-1"
```

So to proceed with the member account we will have to deploy the master account!

> Apply complete! Resources: 20 added, 0 changed, 0 destroyed.

In the member, iterate again.

```
Error: Failed getting S3 bucket: Forbidden: Forbidden
	status code: 403, request id: 96120BB4E7D053AB, host id: wUZJa464AcyhMENVFS8bV5Z5oAhKxXlU4j5Uug+yj0QqAFKoJHQVpj+Psq+NZMRjXMKBAPGx65g= Bucket: "isme-baseline-audit-1"
```

The bucket is "forbidden". Okay, so this is complicated.

## Fixing the audit bucket

Looking at the bucket in the S3 console I see two problems:

* Cross-account access appears to be blocked at the bucket level
* The bucket policy allows the Config service to write only for the delegated
  administrator account (the resource key contains its account ID)

The module has a variable audit_log_bucket_custom_policy_json that in bucket.tf
sets the override_json of the bucket policy that is attached to the bucket
created by the secure-bucket submodule.

https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document

By carefully setting the sid of the custom policy we might be able to open up
the access to AWS Config for all accounts in the organization (see the code
derived from the AWS sample code for an example).

The public access block is created by the secure-bucket submodule
unconditionally.

We can try to patch it out.

Delete all the lines between these two inclusive:

```
resource "aws_s3_bucket_public_access_block" "content" {
...
}
```

This can be done with a sed in-place ranged delete.

In the long run, and if the modifications become more complicated, we might want
to just manage the audit bucket externally.

In the master account, patch the secure-bucket module:

```
sed --in-place=.disabled --expression '/resource "aws_s3_bucket_public_access_block" "content" {/,/}/d' .terraform/modules/secure_baseline/modules/secure-bucket/main.tf
```

Iterate in the master.

> Plan: 0 to add, 0 to change, 1 to destroy.

The access block would be destroyed.

Apply!

What happens now when we try to deploy the member account?

Iterate in the member.

Same "forbidden" error. Maybe it's not the public access block that's the
problem.

What is missing to allow cross account access? Is Terraform making some
forbidden API calls to collect metadata?

Using TF_LOG=trace to figure it out.

```
TF_LOG=trace terraform plan \
  -var audit_s3_bucket_name=isme-baseline-audit-1 \
  -var profile=saa-mem1-iam \
  -out ~/tmp/tfplan \
> ~/tmp/tftrace 2>&1
```

Indeed, Terraform is making a HeadBucket request.

```
2021-02-02T19:26:36.086+0100 [INFO]  plugin.terraform-provider-aws_v3.26.0_x5: 2021/02/02 19:26:36 [DEBUG] [aws-sdk-go] DEBUG: Response s3/HeadBucket Details:
---[ RESPONSE ]--------------------------------------
HTTP/1.1 403 Forbidden
Connection: close
Transfer-Encoding: chunked
Content-Type: application/xml
Date: Tue, 02 Feb 2021 18:26:35 GMT
Server: AmazonS3
X-Amz-Bucket-Region: us-east-1
X-Amz-Id-2: 8xtIaHBdidhPn1y5aT21pycI95oHmGM1HTyW4FdR7SSA5xdmFkm+gDgl744u3rl8//tDH9RsN+g=
X-Amz-Request-Id: 3706269911469B8B



-----------------------------------------------------: timestamp=2021-02-02T19:26:36.085+0100
```

Possibly related Github issue in the Terraform AWS provider: "Replace HeadBucket
request with ListBuckets request for s3 resources". I've subscribed.
https://github.com/hashicorp/terraform-provider-aws/issues/17195

A discussion of the permissions Terraform needs to work by Erick Soen. "Teaching
Terraform from the ground up..."
https://dev.to/ericksoen/teaching-terraform-from-the-ground-up-55nm

Erick's article is great! Thank him after trying out what we learned. (See
message in Dentsu Notes).

* Override the bucket policy to allow its configuration to be read by the member
  accounts, so that Terraform may collect the metadata for its plan.
* If it wasn't actually causing the issue and the setup would work with it in
  place, reinstate the public access block (less hacking, more working!)

Why is the member account reading the bucket?

In bucket.tf:

```terraform
data "aws_s3_bucket" "external" {
  count  = local.use_external_bucket ? 1 : 0
  bucket = var.audit_log_bucket_name
}
```

What S3 API calls are made by the Read function? Clone the Terraform AWS provider and run:

https://github.com/hashicorp/terraform-provider-aws

```
grep -E 'func|s3conn' aws/resource_aws_s3_bucket.go
```

Full output is too big to show here and doesn't give the complete picture anyway
because if you follow all the code paths you have to read called functions in
other parts of the provider and in other repositories.

API calls made in resourceAwsS3BucketRead. Calls made directly from function
body except where noted:

* HeadBucket (also via GetBucketRegionWithClient)
* GetBucketPolicy
* GetBucketAcl
* GetBucketCors
* GetBucketWebsite
* GetBucketVersioning
* GetBucketLifecycleConfiguration
* GetBucketReplication
* GetObjectLockConfiguration (via readS3ObjectLockConfiguration)
* GetBucketTagging (via S3BucketListTags)

Add these permissions to a aws_iam_policy_document passed to the module as
audit_log_bucket_custom_policy_json.

First, set up a minimal test in the mem_perm_test directory.

Currently the test is failing when run like this:

```
$ AWS_DEFAULT_REGION=us-east-1 AWS_PROFILE=saa-mem1-iam terraform plan

Error: Failed getting S3 bucket: Forbidden: Forbidden
	status code: 403, request id: 3D276EDC6EDCB65A, host id: PCxRn611yhwrcxRNgqtsaLkW4EG2R6cEzkJsus88QNjlImE8uiZvpjHVJZpglRZBj4KsaUIthDs= Bucket: "isme-baseline-audit-1"
```

Now edit the main.tf of the master account to add the policy and deploy it.

```
Error: Error putting S3 policy: MalformedPolicy: Policy has invalid action
	status code: 400, request id: D659D71C9AF80A07, host id: s9Cyic/Hzf4B2qIV2kZ4dL40LBF5HXY305TPl61xelAN6qwWAF/c3vuSR9Qi76EYd8G/vASUxRc=
```

Ah! Maybe the function names in Go are not exactly the same as the IAM actions
:sweat_smile:.

Most of the methods use permissions with the same name, but there are
exceptions:

* HeadBucket: s3:ListBucket
* GetBucketLifecycleConfiguration: s3:GetLifecycleConfiguration
* GetBucketReplication: s3:GetReplicationConfiguration
* GetBucketCors: s3:GetBucketCORS (spelling differs only in case, and IAM is
  insensitive)
* GetObjectLockConfiguration: s3:GetBucketObjectLockConfiguration

* S3 API Method list:
  https://docs.aws.amazon.com/AmazonS3/latest/API/API_Operations_Amazon_Simple_Storage_Service.html
* S3 IAM action list:
  https://docs.aws.amazon.com/service-authorization/latest/reference/list_amazons3.html

With the correct IAM actions the new policy is applied.

> Apply complete! Resources: 0 added, 1 changed, 0 destroyed.

Now iterate the member again.

> Plan: 11 to add, 0 to change, 0 to destroy.

The plan works because the audit bucket in the master account can now be read.

Before looking at the plan of the member account in detail, check if the public
access block can be restored.

Revert the module code and patch it without deleting the public access block.

Iterate the master.

> Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

The public access block is back in place.

Can I still run the plan ("iterate") in the member account?

> Plan: 11 to add, 0 to change, 0 to destroy.

Yes!

So, in the end, the audit bucket didn't need to be "fixed", but there were some
missing permissions for Terraform from member into the master. Perhaps the
documentation could be clearer on that.

## Configuring the audit bucket for member accounts

> The bucket policy allows the Config service to write only for the delegated
  administrator account (the resource key contains its account ID)

Yes, but it's because I missed something.

> The member_accounts argument isn't used by the AWS Config submodule.

That's wrong. The module's bucket.tf uses member_accounts to set permissions for
AWS Config log delivery for member accounts. The statements have folling sids:

* AWSConfigBucketDelivery
* AWSConfigBucketPermissionsCheckForMemberAccounts
* AWSConfigBucketExistenceCheckForMemberAccounts
* AWSConfigBucketDeliveryForMemberAccounts

Set member_accounts in the master main.tf.

Iterate in the master account.

```
                      ~ Resource  = "arn:aws:s3:::isme-baseline-audit-1/config/AWSLogs/933189656188/Config/*" -> [
                          + "arn:aws:s3:::isme-baseline-audit-1/config/AWSLogs/933189656188/Config/*",
                          + "arn:aws:s3:::isme-baseline-audit-1/config/AWSLogs/638726906110/Config/*",
                        ]
```

The plan shows that the correct resource prefix for the member account would be
added in the sid AWSConfigBucketDelivery. It also adds new "ForMemberAccounts"
sids.

Apply to master.

> Apply complete! Resources: 0 added, 1 changed, 0 destroyed.

## Deploying the member account

Planned in the member account and deployed without problems!

> Apply complete! Resources: 11 added, 0 changed, 0 destroyed.

## Deploying another member account (organization management account)

I'll add the organization management account ("gen") as another member account.

Workflow:

* Add account to member_accounts list in master main.tf
* Plan and apply master as before
* Plan and apply member with profile saa-gen-iam

Plan command for gen:

```
terraform plan \
  -var audit_s3_bucket_name=isme-baseline-audit-1 \
  -var profile=saa-gen-iam \
  -out ~/tmp/tfplan
```

```
Error: AuthorizationError: User: arn:aws:iam::480783779961:user/iamadmin is not authorized to perform: SNS:GetTopicAttributes on resource: arn:aws:sns:us-east-1:638726906110:ConfigChanges
	status code: 403, request id: b5613361-c1f6-5e73-96e9-6bf5069e2e3a
```

Why didn't that happen with the mem1 account?

Do we need to use a new state file?

```
terraform workspace new gen
```

Run the plan command for gen again:

> Plan: 11 to add, 0 to change, 0 to destroy.

Yes, of course, each account needs its own state.

But wait, why didn't I need to add permissions for Terraform metadata queries
from the organization management account? I had included the mem1 account ID
directly instead of iterating over the member_accounts variable.

Maybe because it's an organization management account? I won't spend time
looking into it if it's not causing problems. I'll just add it like I should
have and move on.

Later we'll see how to set up the backend as it us used in Dentsu (central S3
bucket with subfolder per account).

## Adding a new region

Now we will add the eu-west-1 region.

Add the region identifier to the target_regions list in master main.

Plan in master.

> Plan: 4 to add, 1 to change, 0 to destroy.

The 4 to add are fine, just config resources:

* config_baseline_eu-west-1.aws_sns_topic.config[0]
* config_baseline_eu-west-1.aws_config_delivery_channel.bucket[0]
* config_baseline_eu-west-1.aws_config_configuration_recorder_status.recorder[0]
* config_baseline_eu-west-1.aws_config_configuration_recorder.recorder[0]

The 1 to change is
module.secure_baseline.aws_iam_role_policy.recorder_publish_policy. Its new
policy will be known after apply.

So apply.

Network error in first attempt:

```
Error: Failed describing Configuration Recorder "default" status: SerializationError: failed decoding JSON RPC response
	status code: 200, request id: a1e8238d-cd80-4411-b3f3-c57b113fd876
caused by: read tcp 192.168.1.145:58104->176.32.109.162:443: read: connection reset by peer
```

Need to plan again:

```
  # module.secure_baseline.module.config_baseline_eu-west-1.aws_config_configuration_recorder_status.recorder[0] is tainted, so must be replaced
-/+ resource "aws_config_configuration_recorder_status" "recorder" {
      ~ id         = "default" -> (known after apply)
        name       = "default"
        # (1 unchanged attribute hidden)
    }

Plan: 1 to add, 0 to change, 1 to destroy.
```

The configuration receorder status is "tained" and must be replaced.

It's not an resource in the AWS API sense, but a convenience for programming
Terraform, so let it happen.

Now AWS Config is recording in us-east-1 and eu-west-1 in the security delegate.

The recorder_publish_policy is realized as an inline policy called
Config-Recorder-Policy in the role called Config-Recorder.

It changed because it now allows publication to the SNS topic in the eu-west-1
region.

```
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": "sns:Publish",
            "Resource": [
                "arn:aws:sns:us-east-1:933189656188:ConfigChanges",
                "arn:aws:sns:eu-west-1:933189656188:ConfigChanges"
            ]
        }
```

A separate role for each region could have been used, but they appear to have
designed it this way for simplicity (without reducing security).

I wonder why they used an SNS topic per account and per region instead of a
centralized one. It contradicts ACBP6.

Add the region identifier to the target regions in member main.

Switch to the default workspace (mem1).

Plan with mem1 profile.

```
terraform plan \
  -var audit_s3_bucket_name=isme-baseline-audit-1 \
  -var profile=saa-mem1-iam \
  -out ~/tmp/tfplan
```

> Plan: 4 to add, 1 to change, 0 to destroy.

Same story as before.

Apply:

> Apply complete! Resources: 4 added, 1 changed, 0 destroyed.

Switch to the gen workspace.

Plan with gen profile.

```
terraform plan \
  -var audit_s3_bucket_name=isme-baseline-audit-1 \
  -var profile=saa-gen-iam \
  -out ~/tmp/tfplan
```

> Plan: 4 to add, 1 to change, 0 to destroy.

Same story as before.

Apply:

> Apply complete! Resources: 4 added, 1 changed, 0 destroyed.

And yes, it's working in eu-west-1 now as well.

No more surprises. It's getting boring. That's good!

## Refactoring for S3 backend and multiple states - Attempt 1

Remembering to change workspace and having to pass the parameters each time is a
bit cumbersome, especially for the various member accounts.

In the production system we are using an S3 backend with multiple state files
per account. Can we refactor the solution to work with this setup?

Need to review the Terraform workspace migration documentation. Migrating
multiple workspaces is a complex task. As `terraform init` warns, "Terraform
initialization doesn't currently migrate only select workspaces. If you want to
migrate a select number of workspaces, you must manually pull and push those
states."
 
https://www.terraform.io/docs/cloud/migrate/workspaces.html

That didn't really help.

According to this open issue to rename the workspaces, each workspace is a separate
object in S3.

https://github.com/hashicorp/terraform/issues/16072

So let's just upload all the workspaces and see what happens.

It aquired the state lock twice.

```
Acquiring state lock. This may take a few moments...
Releasing state lock. This may take a few moments...
Acquiring state lock. This may take a few moments...
Releasing state lock. This may take a few moments...
```

All the locks in DynmoDB. Here we can see that the workspace name prefixes
whatever you set as the key name as `/env:worksspace_name/`. Unless it's the
default workspace which is unprefixed.

```
$ AWS_PROFILE=saa-secdel-iam aws dynamodb scan --table-name TerraformBackend-Table-1OMF7F6CBQZ2P --region eu-west-1 --query 'Items[].[LockID.S]' --output text
terraformbackend-bucket-wmrjio8cem8n/terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/env:/mem1/secondary.terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/env:/secdel/administrator.terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/secondary.terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/config/646557742042/terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/administrator.terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/env:/gen/config/646557742042/terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/env:/gen/secondary.terraform.tfstate-md5
```

What happens if I change the key name and run init again?

This time just make up a junk name. The account IDs in the names aren't forced
yet by policy. That and I used the wrong account Id the first time.

Yes, they get uploaded again. So we can delete the extra workspaces when we
duplicate the state locally for a new backend configuration.

```
$ AWS_PROFILE=saa-secdel-iam aws dynamodb scan --table-name TerraformBackend-Table-1OMF7F6CBQZ2P --region eu-west-1 --query 'Items[].[LockID.S]' --output text
terraformbackend-bucket-wmrjio8cem8n/terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/env:/mem1/secondary.terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/env:/secdel/administrator.terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/secondary.terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/config/646557742042/terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/env:/gen/config/1234/terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/administrator.terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/config/1234/terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/env:/gen/config/646557742042/terraform.tfstate-md5
terraformbackend-bucket-wmrjio8cem8n/env:/gen/secondary.terraform.tfstate-md5
```

And the view in S3:

```
$ AWS_PROFILE=saa-secdel-iam aws s3 ls --recursive s3://terraformbackend-bucket-wmrjio8cem8n/
2021-01-28 09:17:09        156 administrator.terraform.tfstate
2021-02-03 20:44:26      59989 config/1234/terraform.tfstate
2021-02-03 20:29:38      59989 config/646557742042/terraform.tfstate
2021-02-03 20:44:32      62507 env:/gen/config/1234/terraform.tfstate
2021-02-03 20:29:44      62507 env:/gen/config/646557742042/terraform.tfstate
2021-02-01 13:54:37        157 env:/gen/secondary.terraform.tfstate
2021-02-01 13:51:19        156 env:/mem1/secondary.terraform.tfstate
2021-02-01 19:17:31        157 env:/secdel/administrator.terraform.tfstate
2021-01-29 15:49:45      26397 secondary.terraform.tfstate
```

I've created a new "mem1" root module that calls the member module. It passes
hard-coded values for all the arguments so that to use iterate it you just run
`terraform plan -out ~/tmp/tfplan`.

> Plan: 42 to add, 0 to change, 0 to destroy.

We are expecting 0 things to be added. 42 are added because I need to patch the
tasb module again. Now that the member module is being used as a submodule, the
.terraform cache folder in the member folder is ignored.

```
$ rename.ul .tf .tf.disabled .terraform/modules/secure_baseline/vpc_baselines.tf
rename.ul: .terraform/modules/secure_baseline/vpc_baselines.tf: not accessible: No existe el archivo o el directorio
```

Oh! The rename command doesn't work because now the path in the cache is different.

The module's code starts at ".terraform/modules/mem1.secure_baseline/".

This time the patch commands, running in the mem1 folder, are:

```
rename.ul .tf .tf.disabled .terraform/modules/mem1.secure_baseline/vpc_baselines.tf
rename.ul .tf .tf.disabled .terraform/modules/mem1.secure_baseline/outputs.tf
cp ../module_injections/outputs.tf .terraform/modules/mem1.secure_baseline/outputs.tf
rename.ul .tf .tf.disabled .terraform/modules/mem1.secure_baseline/ebs_baselines.tf
rename.ul .tf .tf.disabled .terraform/modules/mem1.secure_baseline/analyzer_baselines.tf
rename.ul .tf .tf.disabled .terraform/modules/mem1.secure_baseline/securityhub_baselines.tf
rename.ul .tf .tf.disabled .terraform/modules/mem1.secure_baseline/guardduty_baselines.tf
rename.ul .tf .tf.disabled .terraform/modules/mem1.secure_baseline/main.tf
cp ../module_injections/main.tf .terraform/modules/mem1.secure_baseline/main.tf
```

Looks like there might be pattern here that depends on what I call the instance
of the submodule invocation. When I do this for the gen account we'll see how to
generalize this.

Iterate:

> Plan: 15 to add, 0 to change, 0 to destroy.

It still wants to add 15 things. 

Could be because the state file is empty...

```
$ terraform state list
No state file was found!

State management commands require a state file. Run this command
in a directory where Terraform has been run or use the -state flag
to point the command to a specific state location.
$ terraform workspace
Usage: terraform workspace

  new, list, show, select and delete Terraform workspaces.
$ terraform workspace list
* default

$ ls -l
total 4
-rw-rw-r-- 1 isme isme 652 feb  3 21:16 main.tf
-rw-rw-r-- 1 isme isme   0 feb  3 21:06 terraform.tfstate
```

Well, checking my command history, I copied the wrong state file, and it happens
to be empty.

```
 2206  ls terraform.tfstate.d/
 2207  cp terraform.tfstate.d/gen/terraform.tfstate mem1/terraform.tfstate
 2208  cp terraform.tfstate.d/gen/terraform.tfstate ../mem1/terraform.tfstate
```

```
$ ls -lR ../member/terraform.tfstate.d/
../member/terraform.tfstate.d/:
total 4
drwxr-xr-x 2 isme isme 4096 feb  3 20:29 gen

../member/terraform.tfstate.d/gen:
total 64
-rw-rw-r-- 1 isme isme     0 feb  3 20:29 terraform.tfstate
-rw-rw-r-- 1 isme isme 62507 feb  3 20:29 terraform.tfstate.backup
```

Where is the mem1 state file? Ah, yes, it was the default state file. But it's
empty too. Because of the upload to S3?

```
$ ls -l ../member/terraform.tfstate
-rw-rw-r-- 1 isme isme 0 feb  3 20:29 ../member/terraform.tfstate
```

Phew, disabling the S3 backend by commenting it out and running it again gets it
back to my local.

```
$ terraform init
Initializing modules...

Initializing the backend...
Terraform has detected you're unconfiguring your previously set "s3" backend.
Do you want to migrate all workspaces to "local"?
  Both the existing "s3" backend and the newly configured "local" backend
  support workspaces. When migrating between backends, Terraform will copy
  all workspaces (with the same names). THIS WILL OVERWRITE any conflicting
  states in the destination.
  
  Terraform initialization doesn't currently migrate only select workspaces.
  If you want to migrate a select number of workspaces, you must manually
  pull and push those states.
  
  If you answer "yes", Terraform will migrate all states. If you answer
  "no", Terraform will abort.

  Enter a value: yes

Releasing state lock. This may take a few moments...
Releasing state lock. This may take a few moments...


Successfully unset the backend "s3". Terraform will now operate locally.

Initializing provider plugins...
- Reusing previous version of hashicorp/aws from the dependency lock file
- Using hashicorp/aws v3.26.0 from the shared cache directory

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
$ find -name terraform.tfstate | xargs ls
./.terraform/terraform.tfstate	./terraform.tfstate  ./terraform.tfstate.d/gen/terraform.tfstate
$ find -name terraform.tfstate | xargs ls -l
-rw-rw-r-- 1 isme isme   282 feb  3 21:39 ./.terraform/terraform.tfstate
-rw-rw-r-- 1 isme isme 59989 feb  3 21:39 ./terraform.tfstate
-rw-rw-r-- 1 isme isme 62507 feb  3 21:39 ./terraform.tfstate.d/gen/terraform.tfstate
```

So now in the mem1 folder I also comment out the backend in mem1 main and init again.

And to copy the mem1 state I do:

```
cp ../member/terraform.tfstate .
```

Now terraform plan

```
Error: Provider configuration not present

To work with
module.secure_baseline.module.config_baseline_eu-west-1.aws_config_configuration_recorder.recorder[0]
(orphan) its original provider configuration at
provider["registry.terraform.io/hashicorp/aws"].eu-west-1 is required, but it
has been removed. This occurs when a provider configuration is removed while
objects created by that provider still exist in the state. Re-add the provider
configuration to destroy
module.secure_baseline.module.config_baseline_eu-west-1.aws_config_configuration_recorder.recorder[0]
(orphan), after which you can remove the provider configuration again.

```

Damn, all the resources look like this. Do I have to duplicate the the provider
settings? Or have I messed something up?

Try copying and pasting the providers boilerplate (I hope it's not this).

After adding all the boilerplate to my mem1 module, the plan works.

> Plan: 15 to add, 0 to change, 15 to destroy.

At least the add and destroy counts match!

I would need to import the resources to the new addresses. Worth it? Or should I
just delete and rebuild?

If I do need to include the boilerplate at every level, so much abstraction
becomes less appealing.

It's getting late so I'm going to sleep on it and call Eliese.

## Refactoring attempt 2

The requirement to copy the boilerplate everywhere combined with the patching
complexity makes more than one level of abstraction over the module itself
unappealing.

I'm going to revert everything and do it again but as simple as possible.

I've destroyed everything that was deployed via the master and member folders.

I've reverted the first refactoring attempt.

The new refactoring should have the the following design goals in mind:

* no more than 1 level of abstraction over the module being used to avoid
  tedious boilerplate
* instead of master/member modules, one folder for each account
* so master becomes "security-delegate"
* we make a copy of member and name one "mem1" and the other "gen".
* all the modules representing accounts should set sensible defaults for their
  variables so that they can by planned and applied without setting variables on
  the command line. these variables will be in variables.tf
* the file main.tf of each "member" account should be identical - can this be
  enforced by scripting or symlinking (does anyone who would run this use
  windows?)
* each member account module will have its own backend.tf. the default workspace
  will be used so that there's no prefixes generated by workspace names.
* the role in the terraform backend will restrict each member account to a
  prefix containing the account ID. I think it's it's the only value that
  already exists that the account owner can't change (or in the role policy we
  smap account numbers to strings as in production)
* the patching should be scripted

Move existing member to member_model. To create new member accounts we will copy
this folder and change values in the copy.

Refactored according to the design goals.

New terminology:

* master: security delegate
* member: member_model

### Testing the security delegate

Now we plan and apply the security delegate like this:

```
./init_and_patch_module.bash delegated_admin/

(cd delegated_admin/ && terraform plan -out ~/tmp/tfplan)
```

Plan:

> Plan: 24 to add, 0 to change, 0 to destroy.

Apply:

```

Error: Error putting S3 policy: OperationAborted: A conflicting conditional operation is currently in progress against this resource. Please try again.
	status code: 409, request id: FT8J1Q0W6JCJBNEY, host id: CyNNnM255GBfRAuiwPQWDcvCd0dWD2WHPd4FiEYaQ2Oz8xZRqk+cPeEklCGdLGRZRO5Za81i+pE=
```

Plan again:

> Plan: 24 to add, 0 to change, 0 to destroy.

That's odd.

```
$ terraform apply ~/tmp/tfplan

Error: Saved plan is stale
```

That's a mess. Something has gone really wrong with the state file.

```
$ ls -l delegated_admin/
total 128
-rw-rw-r-- 1 isme isme   2674 feb  4 12:32 main.tf
-rw-rw-r-- 1 isme isme   1946 feb  2 14:48 regions.tf
-rw-rw-r-- 1 isme isme    157 feb  3 23:59 terraform.tfstate
-rw-rw-r-- 1 isme isme 107778 feb  3 23:49 terraform.tfstate.backup
-rw-rw-r-- 1 isme isme    959 feb  4 09:45 variables.tf
-rw-rw-r-- 1 isme isme     46 feb  2 13:12 versions.tf
```

Try reusing the backed up one with more data in it.

```
(cd delegated_admin/ && mv terraform.tfstate terraform.tfsate.stale && mv terraform.tfstate.backup terraform.tfstate)
```

Plan:

> Plan: 2 to add, 2 to change, 0 to destroy.

Looks better.

> Apply complete! Resources: 2 added, 2 changed, 0 destroyed.

Done.

NOTE: Running `terraform plan` outside the delegated_admin directory was wrong.
It should be run inside the directory. If it appeared to work it was because of
unintended behaviors in other commands there were discovered and documented
later.

### Testing the member model

Now we configure a new member account like this:

```
cp -r member_model/ mem1
vim mem1/variables.tf
```

Test init and patch again after some changes because of terraform init
weirdness. It writes the .terraform files to the working directory even if you
pass it a different directory to operate on.


```
./init_and_patch_module.bash mem1
```

Before:

```
$ ls -la
total 84
drwxrwxr-x  8 isme isme  4096 feb  4 17:35 .
drwxrwxr-x 14 isme isme  4096 feb  2 13:11 ..
drwxrwxr-x  3 isme isme  4096 feb  4 17:10 delegated_admin
drwxrwxr-x  8 isme isme  4096 feb  4 17:36 .git
-rw-rw-r--  1 isme isme    76 feb  2 20:04 .gitignore
-rwxrwxr-x  1 isme isme   986 feb  4 17:35 init_and_patch_module.bash
drwxrwxr-x  2 isme isme  4096 feb  4 17:36 mem1
drwxrwxr-x  2 isme isme  4096 feb  4 17:32 member_model
drwxrwxr-x  3 isme isme  4096 feb  3 12:56 mem_perm_test
drwxrwxr-x  2 isme isme  4096 feb  2 17:35 module_injections
-rw-rw-r--  1 isme isme 40059 feb  4 17:16 paring-down-the-module.md
-rw-rw-r--  1 isme isme  2464 feb  2 13:14 README.md
$ ls -la mem1
total 24
drwxrwxr-x 2 isme isme 4096 feb  4 17:36 .
drwxrwxr-x 8 isme isme 4096 feb  4 17:35 ..
-rw-rw-r-- 1 isme isme 1296 feb  4 17:35 main.tf
-rw-rw-r-- 1 isme isme 2048 feb  4 17:35 regions.tf
-rw-rw-r-- 1 isme isme  540 feb  4 17:36 variables.tf
-rw-rw-r-- 1 isme isme   46 feb  4 17:35 versions.tf
```

After:

```
$ ls -la
total 88
drwxrwxr-x  8 isme isme  4096 feb  4 17:35 .
drwxrwxr-x 14 isme isme  4096 feb  2 13:11 ..
drwxrwxr-x  3 isme isme  4096 feb  4 17:10 delegated_admin
drwxrwxr-x  8 isme isme  4096 feb  4 17:36 .git
-rw-rw-r--  1 isme isme    76 feb  2 20:04 .gitignore
-rwxrwxr-x  1 isme isme   986 feb  4 17:35 init_and_patch_module.bash
drwxrwxr-x  3 isme isme  4096 feb  4 17:38 mem1
drwxrwxr-x  2 isme isme  4096 feb  4 17:32 member_model
drwxrwxr-x  3 isme isme  4096 feb  3 12:56 mem_perm_test
drwxrwxr-x  2 isme isme  4096 feb  2 17:35 module_injections
-rw-rw-r--  1 isme isme 41504 feb  4 17:38 paring-down-the-module.md
-rw-rw-r--  1 isme isme  2464 feb  2 13:14 README.md
$ la -la mem1
total 32
drwxrwxr-x 3 isme isme 4096 feb  4 17:38 .
drwxrwxr-x 8 isme isme 4096 feb  4 17:35 ..
-rw-rw-r-- 1 isme isme 1296 feb  4 17:35 main.tf
-rw-rw-r-- 1 isme isme 2048 feb  4 17:35 regions.tf
drwxrwxr-x 4 isme isme 4096 feb  4 17:38 .terraform
-rw-r--r-- 1 isme isme  283 feb  4 17:38 .terraform.lock.hcl
-rw-rw-r-- 1 isme isme  540 feb  4 17:36 variables.tf
-rw-rw-r-- 1 isme isme   46 feb  4 17:35 versions.tf
$ la -la mem1/.terraform/modules/secure_baseline/*.disabled
-rw-rw-r-- 1 isme isme  5691 feb  4 17:38 mem1/.terraform/modules/secure_baseline/analyzer_baselines.tf.disabled
-rw-rw-r-- 1 isme isme  3075 feb  4 17:38 mem1/.terraform/modules/secure_baseline/ebs_baselines.tf.disabled
-rw-rw-r-- 1 isme isme  9702 feb  4 17:38 mem1/.terraform/modules/secure_baseline/guardduty_baselines.tf.disabled
-rw-rw-r-- 1 isme isme  3564 feb  4 17:38 mem1/.terraform/modules/secure_baseline/main.tf.disabled
-rw-rw-r-- 1 isme isme 13706 feb  4 17:38 mem1/.terraform/modules/secure_baseline/outputs.tf.disabled
-rw-rw-r-- 1 isme isme  8707 feb  4 17:38 mem1/.terraform/modules/secure_baseline/securityhub_baselines.tf.disabled
-rw-rw-r-- 1 isme isme 13247 feb  4 17:38 mem1/.terraform/modules/secure_baseline/vpc_baselines.tf.disabled
```

Okay, it's working as I expected.

I think I might have deleted the security delegate state file, though. :-(

Now to test the planning and applying.

Plan:

```
(cd mem1/ && terraform plan -out ~/tmp/tfplan)
```

> Plan: 15 to add, 0 to change, 0 to destroy.

Apply:

```
(cd mem1 && terraform apply ~/tmp/tfplan)
```

> Apply complete! Resources: 15 added, 0 changed, 0 destroyed.

It's working for one member!

What about another?

If this works without a hitch then we can figure out moving it to the backend.

I found a backend state file in the security_delegate folder. It's almost in
sync so I just used it.

> Apply complete! Resources: 2 added, 1 changed, 0 destroyed.

Now I'll do the "gen" member account.

```bash
cp -r member_model/ gen
vim gen/variables.tf
./init_and_patch_module.bash gen
(cd gen/ && terraform plan -out ~/tmp/tfplan)
(cd gen/ && terraform apply ~/tmp/tfplan)
```

Plan:

> Plan: 15 to add, 0 to change, 0 to destroy.

Apply:

> Apply complete! Resources: 15 added, 0 changed, 0 destroyed.

## Adding a new member account

The "SSO" account was added under the belief that SSO supported delegated
administrator accounts. It doesn't, but we can still use this account to
exercise the member_accounts variable of the delegated_admin module.

To set up the SSO account, first we need to add its account ID to the default of
the member_accounts variable.

Get the account ID from the Organization console while logged into the
organization management account.

```
vim delegated_admin/variables.tf
```

Add the account ID and an empty email address. The module expects an email
address but would use it only to set up GuardDuty, which we've disabled.

Plan and apply the delegated admin.

```
(cd delegated_admin/ && terraform plan -out ~/tmp/tfplan)
(cd delegated_admin/ && terraform apply ~/tmp/tfplan)
```

Plan:

> module.secure_baseline.aws_s3_bucket_policy.audit_log[0] will be updated in-place

> Plan: 0 to add, 1 to change, 0 to destroy.

The plan shows that the new account ID will be added to the bucket policy.

Apply

> Apply complete! Resources: 0 added, 1 changed, 0 destroyed.

Create a CLI profile for the new account if you haven't already. If you copy an
existing profile for another account you need to change the name and the ARN.

From here the process is the same as before with the gen account.

```bash
cp -r member_model/ sso
vim sso/variables.tf
./init_and_patch_module.bash sso
(cd sso/ && terraform plan -out ~/tmp/tfplan)
(cd sso/ && terraform apply ~/tmp/tfplan)
```

Plan:

> Plan: 15 to add, 0 to change, 0 to destroy.

Apply error:

```text
Error: Error creating IAM Role Config-Recorder: EntityAlreadyExists: Role with name Config-Recorder already exists.
	status code: 409, request id: 18dc5173-10d2-4116-a470-880d936ddb41
```

The role doesn't appear in the IAM console.

It does appear in the CLI.

```text
$ aws iam list-roles --profile saa-sso-iam --query 'Roles[].RoleName'
[
    "AWSReservedSSO_AdministratorAccess_d2c6e1a568becd2e",
    "AWSServiceRoleForCloudFormationStackSetsOrgMember",
    "AWSServiceRoleForConfigConforms",
    "AWSServiceRoleForConfigMultiAccountSetup",
    "AWSServiceRoleForOrganizations",
    "AWSServiceRoleForSecurityHub",
    "AWSServiceRoleForSSO",
    "AWSServiceRoleForSupport",
    "AWSServiceRoleForTrustedAdvisor",
    "Config-Recorder",
    "ConfigRecorderRole",
    "OrganizationAccountAccessRole",
    "stacksets-exec-fa60efe9956411b69f7d4caeb2d2b547"
]
```

The account ID in the CLI profile was not that of the SSO account! I had copied
the config section for the mem1 account without updating the ARN.

Before updating the ARN, destroy anything that did get created in the wrong account.

Terraform created a state file for the partial application.

```
$ terraform state list
data.aws_caller_identity.current
module.secure_baseline.data.aws_iam_policy_document.config_organization_assume_role_policy
module.secure_baseline.data.aws_iam_policy_document.recorder_assume_role_policy
module.secure_baseline.data.aws_iam_policy_document.recorder_publish_policy
module.secure_baseline.data.aws_organizations_organization.org[0]
module.secure_baseline.data.aws_s3_bucket.external[0]
module.secure_baseline.module.config_baseline_eu-west-1.aws_sns_topic.config[0]
module.secure_baseline.module.config_baseline_us-east-1.aws_sns_topic.config[0]
```

Two SNS topics were created called ConfigChanges.

Why were these not already created?

Actually, they were already created correctly with the mem1 deployment.

And they were created again without conflict with the sso deployment.

```text
$ (cd mem1 && terraform state pull | gron | grep -P "attributes\.arn.*ConfigChanges")
json.resources[16].instances[0].attributes.arn = "arn:aws:sns:eu-west-1:638726906110:ConfigChanges";
json.resources[20].instances[0].attributes.arn = "arn:aws:sns:us-east-1:638726906110:ConfigChanges";
$ (cd sso && terraform state pull | gron | grep -P "attributes\.arn.*ConfigChanges")
json.resources[15].instances[0].attributes.arn = "arn:aws:sns:eu-west-1:638726906110:ConfigChanges";
json.resources[19].instances[0].attributes.arn = "arn:aws:sns:us-east-1:638726906110:ConfigChanges";
```

It turns out the the SNS create-topic API is idempotent.

```text
$ for i in {1..3}; do aws sns create-topic --name ConfigChanges --profile saa-sso-iam --region eu-west-1; done
{
    "TopicArn": "arn:aws:sns:eu-west-1:638726906110:ConfigChanges"
}
{
    "TopicArn": "arn:aws:sns:eu-west-1:638726906110:ConfigChanges"
}
{
    "TopicArn": "arn:aws:sns:eu-west-1:638726906110:ConfigChanges"
}
```

So ¡don't! delete the existing resources, just delete the state file and fix the
profile. Everything else in the state file is data and can be deleted safely.

Fixed the profile.

Okay, plan and apply again.

Plan:

> Plan: 15 to add, 0 to change, 0 to destroy.

Apply:

> Apply complete! Resources: 15 added, 0 changed, 0 destroyed.

It's all working!

Aside: Looking at the code, there is an enabled variables for the
config_baseline submodule. It's still unclear how to selectively enable
submodules from the top level as there doesn't appear to be a variable for each
submodule.

## Setting up SSO access

The Terraform AWS provider as of version 3.26.0 now supports AWS SSO access. See
[the Github issue](https://github.com/hashicorp/terraform-provider-aws/issues/10851).

Create a CLI profile for the SSO account via AWS SSO (now the name means
something again!)

```text
$ aws configure sso --profile saa-sso
SSO start URL [None]: https://d-936707a5b2.awsapps.com/start                                             
SSO Region [None]: eu-west-1                                                                             
There are 5 AWS accounts available to you.
Using the account ID 423811555754
The only role available to you is: AdministratorAccess
Using the role name "AdministratorAccess"
CLI default client Region [None]:                                                                        
CLI default output format [None]:                                                                        
```

Edit versions.tf of the sso module to specify v3.26.0 as the minimum version of
the aws provider using the [required_providers syntax](*
https://www.terraform.io/docs/language/providers/requirements.html).

Edit variables.tf of the sso module to use the new CLI profile.

Init and patch the module.

```
Initializing provider plugins...
- Reusing previous version of hashicorp/aws from the dependency lock file
- Using hashicorp/aws v3.27.0 from the shared cache directory
```

It actually used v3.27.0. The changelog says it fixed a bug in SSO profile
handling.

* https://github.com/hashicorp/terraform-provider-aws/blob/v3.27.0/CHANGELOG.md
* https://github.com/hashicorp/terraform-provider-aws/issues/17370

Plan:

```
$ (cd sso/ && terraform plan -out ~/tmp/tfplan)
module.secure_baseline.module.config_baseline_eu-west-1.aws_sns_topic.config[0]: Refreshing state... [id=arn:aws:sns:eu-west-1:423811555754:ConfigChanges]
[...]]
No changes. Infrastructure is up-to-date.
```

It was able to use the new profile! Awesome! That means I don't need an IAM user
as the source profile for Terraform deployments.

Save a copy of the CLI profile configurations and validate them.

This is a quick and dirty validation script.

```text
crudini --get aws-cli-profiles.ini | \
awk '{print $2}' | \
AWS_CONFIG_FILE=aws-cli-profiles.ini parallel \
--keep-order \
--tag \
aws sts get-caller-identity --profile
```

Sample output:

```
saa-gen-iam	{
saa-gen-iam	    "UserId": "AIDAW74HSJB42FLSGFCOD",
saa-gen-iam	    "Account": "480783779961",
saa-gen-iam	    "Arn": "arn:aws:iam::480783779961:user/iamadmin"
saa-gen-iam	}
saa-secdel-iam	{
saa-secdel-iam	    "UserId": "AROA5SRTQDZ6A35OTB5RT:botocore-session-1612544482",
saa-secdel-iam	    "Account": "933189656188",
saa-secdel-iam	    "Arn": "arn:aws:sts::933189656188:assumed-role/OrganizationAccountAccessRole/botocore-session-1612544482"
saa-secdel-iam	}
saa-mem1-iam	{
saa-mem1-iam	    "UserId": "AROAZJNYXED7D3CWXN6II:botocore-session-1612544487",
saa-mem1-iam	    "Account": "638726906110",
saa-mem1-iam	    "Arn": "arn:aws:sts::638726906110:assumed-role/OrganizationAccountAccessRole/botocore-session-1612544487"
saa-mem1-iam	}
saa-sso-iam	{
saa-sso-iam	    "UserId": "AROAWFLJDEGVNREFPFLOS:botocore-session-1612544492",
saa-sso-iam	    "Account": "423811555754",
saa-sso-iam	    "Arn": "arn:aws:sts::423811555754:assumed-role/OrganizationAccountAccessRole/botocore-session-1612544492"
saa-sso-iam	}
saa-gen	
saa-gen	The SSO session associated with this profile has expired or is otherwise invalid. To refresh this SSO session run aws sso login with the corresponding profile.
saa-secdel	
saa-secdel	The SSO session associated with this profile has expired or is otherwise invalid. To refresh this SSO session run aws sso login with the corresponding profile.
saa-mem1	
saa-mem1	The SSO session associated with this profile has expired or is otherwise invalid. To refresh this SSO session run aws sso login with the corresponding profile.
saa-sso	{
saa-sso	    "UserId": "AROAWFLJDEGVAGNQ7BU2E:iain",
saa-sso	    "Account": "423811555754",
saa-sso	    "Arn": "arn:aws:sts::423811555754:assumed-role/AWSReservedSSO_AdministratorAccess_d51fbb30d14441d8/iain"
saa-sso	}
```

The SSO sessions that have expired are at least recognized as SSO sessions, so
syntactically it looks like the config file is valid.

## Migrating the state to the S3 backend

First create a new AWS account for the Terraform backend.

Log into the organization management account.

Create the new "Terraform Backend" account in the Organizations console.

Set up AdministratorAccess in the AWS SSO console.

Create a CLI profile for the account called saa-tfb.

Create a SAM config file to allow the deployment to happen without a cluttered
command line.

Use it to deploy the TerraformBackend stack there.

```bash
(cd terraform_backend && sam deploy --profile saa-tfb)
```

Deployment worked. Along the way I tripped up on a bug in the `sam validate`
command. It ignores the region in a valid samconfig.toml and so fails
validation. https://github.com/aws/aws-sam-cli/issues/1932

### Restricting access to a subfolder per AWS account

All accounts in the organization are permitted to assume a role that grants the
necessary S3 bucket permissions and DynamoDB table permissions specified by the
[Terraform S3 backend documentation](https://www.terraform.io/docs/language/settings/backends/s3.html).

I'd like to limit each client account to its own subfolder, but I see no way to
do this succinctly or dynamically within a single policy because a "caller
account ID" IAM policy variable does not exist.
([IAM policy elements: Variables and tags](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_variables.html#policy-vars-infotouse))

We would have to list out all the principals with their subfolder somehow.
Eventaully we will hit a limit.

* The size of each managed policy cannot exceed 6,144 characters.
  ([IAM and STS quotas](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_iam-quotas.html))
* Bucket policies are limited to 20 KB in size.
  ([Bucket policies and user policies](https://docs.aws.amazon.com/AmazonS3/latest/dev/using-iam-policies.html))
* Quota of customer managed policies in an AWS account: 1500 (default), 5000 (max)
* Quota of roles in an AWS account: 1000 (default), 5000 (max)

It seems the way that gives the most room to grow is to generate a separate role
and policy for each client account. We can take advantage of CloudFormation
nested stacks to reduce the repetition here (if we keep using CloudFormation for
this).

For now I'll just have one shared role that allows writing to any path in the
bucket. I'll trust the client accounts to configure their prefix correctly. I'm
in control of all the client accounts.

Run a command like this in the organization management account to list all the
accounts in the organization.

```bash
aws organizations list-accounts \
--profile saa-gen \
--query 'Accounts[].[Id, Name]' \
--output text
```

### Setting up the S3 backend for the delegated_admin

I added a backend configurations to the delegated_admin.

Now when I run the plan command I get an error because I need to reinitialize.

```
$ (cd delegated_admin/ && terraform plan -out ~/tmp/tfplan)
Backend reinitialization required. Please run "terraform init".
Reason: Initial configuration of the requested backend "s3"
```

Will this work with my init and patch script?

```
$ terraform init
Initializing modules...

Initializing the backend...

Error: error configuring S3 Backend: no valid credential sources for S3 Backend found.

Please see https://www.terraform.io/docs/backends/types/s3.html
for more information about providing credentials.

Error: NoCredentialProviders: no valid providers in chain. Deprecated.
	For verbose messaging see aws.Config.CredentialsChainVerboseErrors
```

Not yet. I'm using an SSO profile for the backend, which is unsupported in
version 0.14.5.

The feature has just been released in version 0.14.6.

So I need to update terraform.

```
apt install terraform
```

After upgrading Terraform it works with SSO profile.

```text
$ terraform init
Initializing modules...

Initializing the backend...
Do you want to copy existing state to the new backend?
  Pre-existing state was found while migrating the previous "local" backend to the
  newly configured "s3" backend. No existing state was found in the newly
  configured "s3" backend. Do you want to copy this state to the new "s3"
  backend? Enter "yes" to copy and "no" to start with an empty state.

  Enter a value: 

```

Will it work?

```
Releasing state lock. This may take a few moments...

Error: Error copying state from the previous "local" backend to the newly configured 
"s3" backend:
    failed to upload state: AccessDenied: Access Denied
	status code: 403, request id: 6674C560BAA8676C, host id: q8Rn88jN9n4j0hSO4vEEzFypuy5uzb/iNE7QtIlRdF6N13SwDLgV8r0dP1qhrvMZK4Y1eVJgdQ8=

The state in the previous backend remains intact and unmodified. Please resolve
the error above and try again.
```

No, it failed!

Was it because I left out the ACL setting?

Yes. Adding `acl = "bucket-owner-full-control"` made it work.

And now the plan output:

> No changes. Infrastructure is up-to-date.

Migration of security_delegate complete!

A summary of the steps (most of these are actually SSO migration steps):

* Define SSO profile saa-secdel
* Upgrade Terraform to 0.14.6 (this can also be specified in versions.tf)
* Ensure the provider is at least 3.26.0 in versions.tf
* Use SSO profile for module provider
* Use SSO profile for S3 backend
* Configure S3 backend

### Migrating to S3 backend for members

We are going to migrate each account's backend separately, duplicating the
effort of editing each repo.

Once they are all in S3, we can try to refactor the code to reduce duplication
by using partial backend configuration.

#### mem1

* Create an SSO profile for mem1
* Copy versions.tf from delegated_admin (to make Terraform aware of SSO)
* Copy backend.tf from delegated_admin
* Change profile and key settings in backend.tf (to use SSO and a unique prefix)
* Use SSO profile in variables.tf
* Use `terraform init -upgrade` in the mem1 module to ensure the right provider
  version will be used and to actually migrate the state to the S3 backend
* Use the init and patch script to patch the module
* Run plan and check that no changes would be applied

### gen

The gen account used to have an IAM user with access keys for accessing the
account. Now that Terraform works with SSO the IAM user iamadmin is no longer necessary.

I've disabled console access for iamadmin and deactived the access key.

Follow the same steps as before. Log into the existing SSO profile for gen first.

### sso

Follow the same steps as for the gen account.

S3 backend done!

## Using KMS in DynamoDB

Not sure how this works with Terraform. Let's find out.

Using the default AWS-managed CMK has no effect of Terraform. Terraform still works.

It will incur KMS costs. It stops cfn_nag nagging me about it.

## Using KMS in S3

This is more complicated that it first seemed.

When you enable server-side encryption on an S3 bucket, what you are really
enabling is per-object default behavior. The default behavior is used if a
PutObject call doesn't specify encryption.

I've set a KMS key for server-side encryption. I haven't granted permissions to
Terraform yet because I want to see it fail with a permissions error.

If you have "encrypted = true" in a Terraform backend configuration without a
corresponding kms_key_id, Terraform sets the encryption option for AES-256, and
so avoids needing permission for the KMS key.

I want my S3 bucket to store inly objects encrypted by my KMS key. It turns out
there's a
[premium support article for that](https://aws.amazon.com/es/premiumsupport/knowledge-center/s3-bucket-store-kms-encrypted-objects/).

To test this I will change the name of the key of the S3 object holding the
state of the sso account and running `terraform init` to copy the state.

I simplified the bucket policy in the support article and included an extra
condition in the role to require the aws:kms encryption method.

Now change the name of the object again and reinit.

```text
Error: Error copying state from the previous "s3" backend to the newly configured 
"s3" backend:
    failed to upload state: AccessDenied: Access Denied
	status code: 403, request id: 7C8226F2BD43F3CE, host id: X/MantrMdE95OGQEeQFJht5bQjrFkMMIcUS7mV2LNq6JV53Iu3GmqYIr4QOokFluHV/FLrGA+kI=

The state in the previous backend remains intact and unmodified. Please resolve
the error above and try again.
```

It does fail to modify the state!

Now add kms_key_id to the sso backend config.

It fails again with another access denied error.

Is it now because of the missing key permissions?

After adding the key permissions, it works!

It makes me a bit uneasy putting all the permissions in the role. It means in
theory another user in the AWS account of the bucket (most likely a careless me)
could still put objects in the bucket that don't meet the requirements.

Actually no one else should be putting objects in that bucket. Can I enforce the
use of that role with the bucket policy? I'll save it for later.

But the sso account is now working with KMS! Great.

## Refactor solution using partial backend configuration

We'll need to fix the config of all the other client accounts now that the role
has changed.

But let's refactor it to use the the KMS config in SSO for all the clients.

TODO (see note below)

* TODO: refactor permissions between bucket policy for forcing the use of the
  KMS key in the bucket policy and then the specific permissions for Terraform
  in the role.
* TODO: Use partial configuration to reduce config duplication. No more one
  folder per account!
* TODO: Enable S3 object logging and KMS key logging in CloudTrail for debugging
  OR enable S3 access logs
  https://github.com/ozbillwang/terraform-best-practices#manage-s3-backend-for-tfstate-files
* TODO: contact the maintainers for how to selectively enable submodules (it
  would have saved all this research)
  https://github.com/hashicorp/terraform-provider-aws/issues/10851
* TODO: Generate a separate access role for each client account
* TODO: use this terraform module to create the backend. also by nozaq
  https://registry.terraform.io/modules/nozaq/remote-state-s3-backend/aws/latest
* TODO: fix S3 "conflicting conditional operation" when creating delegated_admin
  https://discuss.hashicorp.com/t/how-to-resolve-error-s3-policy-operationaborted-conflicting-conditional-operation-is-currently-in-progress/5900

## Using Terraform to manage the Terraform state bucket

After reviewing with my client it was decided to use Terraform to manage the
Terraform state bucket. At first this seems like a chicken-and-egg problem.

Here's how we can solve it:

* Use a module to set up remote backend (nozaq's or another?)
* Initially use a local backend to store the state for that S3 bucket
* After the initial deployment move the state for the bucket to itself

Searching for modules here:

https://registry.terraform.io/search/modules?provider=aws&q=remote

Options:

* https://registry.terraform.io/modules/Cloud-42/remote-state-locking-table/aws/latest (DynamoDB only.)
* https://registry.terraform.io/modules/nozaq/remote-state-s3-backend/aws/latest
  (S3 and DynamoDB. replicates, but maybe we can use the same region. dynamodb is pay per use)
* https://registry.terraform.io/modules/bincyber/remote-state/aws/latest S3 and DynamoDB. single region. You need to specify read capacity units. Unclear if on-demand mode is supported

Nozaq's module seems to be the most mature and closest to what we need.

Test it with same-region replication.

See [tfb README](tfb/README.md).

It works with same-region replication.
